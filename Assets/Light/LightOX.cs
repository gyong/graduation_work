﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightOX : MonoBehaviour {

    [SerializeField]
    private Light[] _Light;

    private Animator _anim;

    private bool _isLighting = true;

    private void FixedUpdate()
    {
        LightONOFF();
    }


	private void LightONOFF()
    {
        if(_isLighting)
        {
            _Light[0].enabled = false;
            _Light[1].enabled = false;
            _Light[2].enabled = false;
            _isLighting = false;
        }
        else if(!_isLighting)
        {
            _Light[0].enabled = true;
            _Light[1].enabled = true;
            _Light[2].enabled = true;
            _isLighting = true;
        }
    }
}
