﻿Shader "Custom/Candle"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}

		_rimColor ("rim", color) = (1,1,1,1)
		_rimPow ("rim 영역 조절", Range(3,10)) = 5
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        #pragma surface surf candle
        #pragma target 3.0

        sampler2D _MainTex;
		fixed4 _Color, _rimColor;
		half _rimPow;

        struct Input
        {
            float2 uv_MainTex;
        };

        void surf (Input IN, inout SurfaceOutput o)
        {
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;
            o.Alpha = c.a;
        }

		float4 Lightingcandle(SurfaceOutput s, float3 lightDir, float3 viewDir, float atten)
		{
			float ndotl = dot(s.Normal, lightDir) *0.5 +0.5;
			float3 diffColor;
			diffColor = ndotl * s.Albedo * atten * _LightColor0;
			
			float3 rimColor;
			float rim = saturate(dot(s.Normal, viewDir));
			rim = pow(1 - rim,_rimPow);
			rimColor = rim * _rimColor;

			float4 final;
			final.rgb = rimColor + diffColor;
			final.a = 1;

			return final;
		}
        ENDCG
    }
    FallBack "Diffuse"
}
