﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using System;


public class Node : IComparable<Node>
{

    public bool walkable;
    public Vector3 worldPosition;
    public int gridX;
    public int gridY;

    public int gCost;
    public int hCost;
    public Node parent;

    public Node(bool _walkable, Vector3 _worldPos, int _gridX, int _gridY)
    {
        this.walkable = _walkable;
        this.worldPosition = _worldPos;
        gridX = _gridX;
        gridY = _gridY;
    }

    public int CompareTo(Node other)
    {
        if (this.fCost < other.fCost)
        {
            return -1;
        }
        else if (this.fCost > other.fCost)
        {
            return 1;
        }
        else
        {
            if (this.gCost < other.gCost)
            {
                return -1;
            }
            else if (this.gCost > other.gCost)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }

    public int fCost
    {
        get { return gCost + hCost; }
    }
}


public class PriorityQueue<T> where T : IComparable<T>
{
    private T[] data;
    private int itemCount;

    public PriorityQueue(int maxSize)
    {
        this.data = new T[maxSize];
        itemCount = 0;
    }

    public bool Contains(T item)
    {
        for (int i = 0; i < itemCount - 1; i++)
        {
            if (data[i].Equals(item))
                return true;
        }

        return false;
    }

    public void Enqueue(T item)
    {
        data[itemCount] = item;
        int ci = itemCount - 1;
        while (ci > 0)
        {
            int pi = (ci - 1) / 2;
            if (data[ci].CompareTo(data[pi]) >= 0)
                break;
            T tmp = data[ci]; data[ci] = data[pi]; data[pi] = tmp;
            ci = pi;
        }

        itemCount++;
    }

    public T Dequeue()
    {
        int li = itemCount - 1;
        T frontItem = data[0];
        data[0] = data[li];

        --li;
        int pi = 0;
        while (true)
        {
            int ci = pi * 2 + 1;
            if (ci > li) break;
            int rc = ci + 1;
            if (rc <= li && data[rc].CompareTo(data[ci]) < 0)
                ci = rc;
            if (data[pi].CompareTo(data[ci]) <= 0) break;
            T tmp = data[pi]; data[pi] = data[ci]; data[ci] = tmp;
            pi = ci;
        }

        itemCount--;
        return frontItem;
    }

    public int Count()
    {
        return itemCount;
    }

    public T Peek()
    {
        T frontItem = data[0];
        return frontItem;
    }

}
