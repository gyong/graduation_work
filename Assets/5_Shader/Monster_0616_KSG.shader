﻿Shader "Custom/Monster/dissolve"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        //_MainTex2 ("RampTex", 2D) = "white" {}

		_ToonLine("ToonLine", Range(0, 1)) = 0.166

		// 슬로우 상태 버튼
		[Toggle]_SlowButton("슬로우 상태",Range(0,1)) = 0

		//림 ==  슬로우
		[HDR]_RimColor("슬로우 상태 컬러", Color) = (0.5,0,0.5,1)
		_RimAmount("Rim Amount", Range(0, 1)) = 0.5
		_RimPow("Rim Pow", Range(0, 1)) = 0.165



		//피격 상태
		_SickColor ("피격상태 컬러", Color) = (0.5,0.25,0.25,1)
		[Toggle]_SickButton("피격 상태",Range(0,1)) = 0


		//사망
		_Height("디졸브 높이", Range(-10,10))= 5
		_NoiseTex("디졸브 텍스쳐", 2D) = "white"{}
		[HideInInspector]_Cutoff("알파테스트", float) = 0.5
		_Dissolve("디졸브 정도" , Range(-1,1)) = 0.5
		[HDR]_LineColor("경계선 색" , color) = (1,1,1,1)
    }
    SubShader
    {
        Tags {"Queue" = "AlphaTest" "IgnoreProjector" = "True" "ForceNoShadowCasting" = "true" "RenderType" = "TransparentCutout""PassFlags" = "OnlyDirectional" }
        LOD 200

		cull back
        CGPROGRAM
        #pragma  surface surf monster alphatest:_Cutoff  keepalpha

        #pragma target 3.0

		float4 _Color;
		sampler2D _MainTex, _MainTex2;

		//rim
		float4 _RimColor;
		float _RimPow, _RimAmount, _SlowButton;
		

		//sick
		float4 _SickColor;
		half _SickButton;

		//디졸브
		sampler2D _NoiseTex;
		float _Dissolve;
		float _Height;

		float4 _LineColor;

        struct Input
        {
			float2 uv_MainTex;
			float2 uv_NoiseTex;
			float2 uv_MainTex2;
			float4 color: COLOR;
			float3 worldPos;
        };


        void surf (Input IN, inout SurfaceOutput o)
        {
			half4 c = tex2D(_MainTex, IN.uv_MainTex);
			half4 noise = tex2D(_NoiseTex, IN.uv_NoiseTex);
			float l = _Height - IN.worldPos.y;

			o.Albedo = c.rgb * _Color.rgb;
			
			float alphalevel = noise.a + _Dissolve;
			o.Alpha = saturate(l)* alphalevel;
			o.Emission = step(0.45, 1 - alphalevel* saturate(l))*_LineColor.rgb * noise;
        }

		float4 Lightingmonster(SurfaceOutput s, float3 lightDir, float3 viewDir, float atten)
		{
			float ndotl = dot(s.Normal, lightDir)*0.5 +0.5;
			float4 ramp = tex2D(_MainTex, float2(ndotl, 0.3));

			float3 diffColor;
			diffColor = s.Albedo;

			//rim
			float rim = 1 - dot(viewDir, s.Normal);
			float rimIntensity = rim * pow(ndotl, _RimPow);
			rimIntensity = smoothstep(_RimAmount - 0.01, _RimAmount + 0.01, rimIntensity);
			float4 rimColor = rimIntensity * _RimColor * _SlowButton;
			
			//sick
			float3 sickColor;
			float sick = saturate(dot(s.Normal, viewDir));
			sickColor.rgb = _SickColor * _SickButton;


			//final
			float4 final;
			final.rgb = saturate(diffColor.rgb +rimColor.rgb + sickColor);
			final.a = s.Alpha;

			return final;
		}
        ENDCG


    }
    FallBack "Diffuse"
}
