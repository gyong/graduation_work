﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CPathUI : MonoBehaviour
{

    private GameObject BossObject, PlayerObject;
    private Transform[] PathPoint;
    
     //ProgressBar_Target = 4 ,  //ProgressBar_Player = 5, // Fill = 1 ,
    private RectTransform[] ProgressbarChlid;
    private Vector3 pBossVelocity;

    private Vector3 BossTransPosition;
    private CBossCHASE pBossState;
    private Vector3[] Calcul;
    private float TotalDisatance , PlayerCalcul;
    private Vector3  PathPointToDistance;
    private float fDistanceLength,  fMaxPerCent ,fDistanceCalul , fPlayerDistance;
    private Pathfinding pPathFinding;
    bool _IsInit = false;
    private void Awake()
    {
       BossObject = GameObject.FindGameObjectWithTag("Boss");
       PlayerObject = GameObject.FindGameObjectWithTag("Player");
       ProgressbarChlid = GameObject.FindGameObjectWithTag("ProgressBar").GetComponentsInChildren<RectTransform>();
     
       pBossState = BossObject.GetComponent<CBossCHASE>();
       pPathFinding = GameObject.Find("AStar").GetComponent<Pathfinding>();


       PathPoint = transform.GetComponentsInChildren<Transform>();
       BossTransPosition  = GameObject.FindGameObjectWithTag("Boss").GetComponent<Transform>().position;
       PathPoint[0].transform.position = BossTransPosition;

       Calcul = new Vector3[PathPoint.Length];

        for ( int i = 0; i <  PathPoint.Length; ++i)
        {
             //print("VectorPos" + i +"번째"+PathPoint[i].transform.position);
             if (i < PathPoint.Length - 1)
             {
                Calcul[i] = PathPoint[i].transform.position -PathPoint[i + 1].transform.position;
                // print(i + " 번째 " + Calcul[i]);
                 PathPointToDistance += Calcul[i];
                 TotalDisatance += Calcul[i].magnitude;
            }
         }

        fMaxPerCent = Vector3.Distance(ProgressbarChlid[2].position, ProgressbarChlid[3].position);
        
    }

   
    private void Update()
    {
        if (BossObject.activeSelf)
        {
            if (pBossState.GetbIsAStar && !_IsInit)
            {
                fDistanceLength = pPathFinding.GetTotalDistance; //월드  최종 거리
                fPlayerDistance = pPathFinding.GetDistance(pPathFinding.GetTempNode[0], pPathFinding.GetTempNode[1]); // 플레이어와 월드의 최종거리
               
                fDistanceCalul = Mathf.Abs(fMaxPerCent / fDistanceLength) * 2; // 비율
                PlayerCalcul = Mathf.Abs(fMaxPerCent / fPlayerDistance) * 2;
                _IsInit = true;
            }

            if (_IsInit)
            {
               
                pBossVelocity = BossObject.GetComponent<Rigidbody>().velocity;
                Vector3 PlayerVelocity = PlayerObject.GetComponent<Rigidbody>().velocity;
                Vector3 Pos = ProgressbarChlid[3].position;
                //Vector3 Pos2 = ProgressbarChlid[4].position;

                if (pBossVelocity.magnitude != 0)
                {
                    Pos.x += (pBossVelocity.magnitude * fDistanceCalul) * Time.deltaTime;
                    ProgressbarChlid[3].position = Pos;
                }
                if(PlayerVelocity.magnitude != 0)
                {
                   // //Input.GetAxis("Horizontal");
                   // pPathFinding.FindPathWithPriorityQueue(PlayerObject.transform.position, GameObject.FindGameObjectWithTag("DestPoint").GetComponent<Transform>().position);
                   // float fCurPlayerDistance = pPathFinding.GetDistance(pPathFinding.GetTempNode[0], pPathFinding.GetTempNode[1]);
                   // //print("현재 " + fCurPlayerDistance + "이동량" + ((fPlayerDistance - fCurPlayerDistance)));
                  
                  
                   //// Pos2.x += ((fPlayerDistance - fCurPlayerDistance) / fCurPlayerDistance);

                   //// ProgressbarChlid[5].position = Pos2;
                   
                }

                //  print(" 이동량 " + pBossVelocity.magnitude * fDistanceCalul);
                // print("유아이 도착지점까지의 거리 " + fMaxPerCent);

            }
        }

    }

  

   
}
