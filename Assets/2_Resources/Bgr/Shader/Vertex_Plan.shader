﻿Shader "Custom/Vertex_Plan"
{
	Properties
	{
		_Color("color", color) = (1,1,1,1)
		_Color2("color", color) = (1,1,1,1)
		_Color3("color", color) = (1,1,1,1)
		_Color4("color", color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_MainTex2("Albedo (RGB)", 2D) = "white" {}
		_MainTex3("Albedo (RGB)", 2D) = "white" {}
		_MainTex4("Albedo (RGB)", 2D) = "white" {}
		_BumpMap("Normalmap",2D) = "bump"{}
		_Sgauge("Smoothness Intensity", float) = 1
		_Ngauge("Normal Intensity", float) = 1
		//_SpecColor ("스펙큘러 컬러", color) = (1,1,1,1)
		//_Specular("specular", Range(0,1)) = 1
		//_Gloss ("gloss", Range(0,1)) = 1
	}
		SubShader
	{
		Tags { "RenderType" = "Opaque" }

		CGPROGRAM
		#pragma surface surf Standard //Lambert //BlinnPhong
		#pragma target 3.0

		sampler2D _MainTex, _MainTex2;
		sampler2D _MainTex3, _MainTex4;
		sampler2D _BumpMap;
		float4 _Color, _Color2, _Color3, _Color4;
		half _Gloss, _Specular, _Sgauge, _Ngauge;

		struct Input
		{
			float2 uv_MainTex, uv_MainTex2;
			float2 uv_MainTex3, uv_MainTex4;
			float4 color:COLOR;
			float2 uv_BumpMap;
		};


		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex)*_Color2;
			fixed4 d = tex2D(_MainTex2, IN.uv_MainTex2);
			fixed4 e = tex2D(_MainTex3, IN.uv_MainTex3)*_Color3;
			fixed4 f = tex2D(_MainTex4, IN.uv_MainTex4)* _Color4;

			fixed3 n = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
			
			o.Albedo = lerp(c.rgb, d.rgb, IN.color.r);
			o.Albedo = lerp(o.Albedo, f.rgb, IN.color.b) * _Color;
			o.Albedo = lerp(o.Albedo, e.rgb, IN.color.g);
		
			//o.Normal = lerp(n.b, float3(n.rg * _Ngauge, n.b), IN.color.g);

			o.Normal = float3(n.rg * _Ngauge, n.b);
			o.Smoothness = IN.color.g *  _Sgauge;
			o.Alpha = c.a;

			//o.Gloss = _Gloss;
			//o.Specular = _Specular;
			//o.Albedo = IN.color.rgb; 
			//o.Smoothness = 0.4*n.r;

		}

		//float4 LightingPlan(SurfaceOutput s, float3 lightDir, float atten)
		//{
		//	//final
		//	float4 final;
		//	final.rgb = s.Albedo;
		//	final.a = 1;
		//	return final;
		//}

		ENDCG
	}
		FallBack "Diffuse"
}