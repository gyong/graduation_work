﻿Shader "Custom/2side"
{
    Properties
    {
        [HDR]_Color("Color",color) = (1,1,1,1)
        _Color2("에미션",color) = (0,0,0,0)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_BumpMap("노말맵", 2D) = "bump"{}
		_Sgauge("스무스니스 강도",Range(0,1)) = 0.5
		_Mgauge("메탈릭 강도",Range(0,1)) = 0

		_Cutoff ("알파 컷오프", Range(0,1)) = 0.5
    }
    SubShader
    {
        Tags {"Queue" = "AlphaTest" "RenderType" = "TransparentCutout" }

		cull off

        CGPROGRAM
        #pragma surface surf Lambert2 alphatest:_Cutoff noambient
        #pragma target 3.0

        sampler2D _MainTex;
        sampler2D _BumpMap;
		fixed4 _Color, _Color2;

		float _Sgauge;
		float _Mgauge;

        struct Input
        {
            float2 uv_MainTex;
            float2 uv_BumpMap;
        };


        void surf (Input IN, inout SurfaceOutput o)
        {
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
			float3 nr = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));

			o.Normal = nr;
            o.Albedo = c.rgb*_Color;
			o.Emission = _Color2;

			o.Alpha = c.a;
        }

		float4 LightingLambert2(SurfaceOutput s, float3 lightDir, float atten) {
			float ndotl = dot(s.Normal, lightDir) *0.7 + 0.3;
			float4 final; 
			final.rgb = s.Albedo * ndotl * atten * _LightColor0.rgb;
				final.a = s.Alpha;
				return final;
		}

        ENDCG
    }
    FallBack "Legacy Shaders/Transparent/Cutout/Diffuse"
}
