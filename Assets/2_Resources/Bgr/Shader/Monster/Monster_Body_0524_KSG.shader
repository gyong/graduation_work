﻿Shader "Unlit/fragmentShader/Monster/Body"
{
	Properties
	{
		//texture
		_Color("Color", Color) = (1,0.88,0.88,1)
		_MainTex("Main Texture", 2D) = "white" {}
		_MainTex2("Main Texture", 2D) = "white" {}

		//outline
		_Outline("아웃라인 두께", Range(0.001, 0.01)) = .002
		_OutlineColor("아웃라인 컬러", color) = (0.55,0,0.4,1)

		//ambient
		[HDR] _AmbientColor("Ambient Color", Color) = (0.52,0.46,0.54,1)

		//ndotl
		_ToonLine("ToonLine", Range(0, 1)) = 0.166

		//rim
		[HDR]_RimColor("Rim Color", Color) = (1,1,1,1)
		_RimAmount("Rim Amount", Range(0, 1)) = 0.5
		_RimPow("Rim Pow", Range(0, 1)) = 0.165

		//슬로우 상태
		[Toggle]_SlowButton("슬로우 상태",Range(0,1)) = 0


		//피격 상태
		_SickColor ("피격상태 컬러", Color) = (0.65,0.65,0.65,1)
		[Toggle]_SickButton("피격 상태",Range(0,1)) = 0

		//dissolve
		_NoiseTex("NoiseTex", 2D) = "white"{}
		_Cut("Alpha Cut", Range(0,1)) = 0
		[HDR]_OutColor("outcolor",Color) = (1.8,0.09,0.15,1)
		_OutThinkness("OutThinkness",Range(1,1.5)) = 1.2
			
	}

		SubShader
		{
			Pass /// 1st pass outline---------------------------------------
			{

			Tags
			{
				 "Queue" = "Transparent" "RenderType" = "Transparent"
			}
			
			ZWrite On
			Blend SrcAlpha OneMinusSrcAlpha
			cull front

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal:NORMAL;
			};

			struct v2f
			{
				float4 color : COLOR;
				float4 vertex:SV_POSITION;
				float3 normal:NORMAL;
			};

			sampler2D _MainTex;
			fixed4 _MainTex_ST;
			uniform float _Outline;
			uniform float4 _OutlineColor;
			half _SlowButton;

			v2f vert(appdata_full v)
			{
				v2f o;
				//float4 clipPosition = UnityObjectToClipPos(position);
				//float3 clipNormal = mul((float3x3) UNITY_MATRIX_VP, mul((float3x3) UNITY_MATRIX_M, normal));
				//clipPosition.xyz += normalize(clipNormal) * _OutlineWidth;
				//return clipPosition;

				o.vertex = UnityObjectToClipPos(v.vertex);
				float3 norm = mul((float3x3) UNITY_MATRIX_MVP, v.normal).xyz;
				float2 offset = normalize(norm.xy) * _Outline * o.vertex.w;
				o.vertex.xy += offset;
				return o;
			}

			//pixel shader
			half4 frag(v2f i) :COLOR
			{
				float4 c;
				c.rgb = float3(0,0,0) + _OutlineColor * _SlowButton;
				c.a = 1;
				return c;
			}
			ENDCG
			}


		Pass   ////////////////////2nd pass /////////////////////
		{
			Tags
			{
				"LightMode" = "ForwardBase"
				"PassFlags" = "OnlyDirectional"
				 "Queue" = "Transparent" "RenderType" = "Transparent"
			}

			cull back

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase

			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float4 uv : TEXCOORD0;

				float3 normal : NORMAL;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;

				float3 worldNormal : NORMAL;
				float3 viewDir : TEXCOORD1;

				SHADOW_COORDS(2)
			};


			sampler2D _MainTex, _MainTex2, _NoiseTex;
			float4 _MainTex_ST, _MainTex2_ST, _NoiseTex_ST;

			//diffuse
			fixed4 _Color;
			fixed4 _AmbientColor;

			//rim
			fixed4 _RimColor;
			half _RimAmount, _RimPow, _SlowButton;

			//toon
			half _ToonLine;

			//dissolve
			half _Cut, _OutThinkness;
			fixed4 _OutColor;

			//sick
			fixed4 _SickColor;
			half _SickButton;

			//vertex-----------------------------------------------------------
			v2f vert(appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);

				o.worldNormal = UnityObjectToWorldNormal(v.normal);
				o.viewDir = WorldSpaceViewDir(v.vertex);

				TRANSFER_SHADOW(o)
				return o;
			}

			//frag-----------------------------------------------------------
			float4 frag(v2f i) : SV_Target
			{
				float3 normal = normalize(i.worldNormal);
				float3 viewDir = normalize(i.viewDir);
				float4 col = tex2D(_MainTex, i.uv);
				float4 mask = tex2D(_MainTex2, i.uv);

				float4 noise = tex2D(_NoiseTex, i.uv);
				float alpha;
				float outline;


				//ndotl------------------------------------
				float ndotl = dot(viewDir, normal);
				ndotl *= mask.a;


				//shadow--------------------------------------
				float shadow = SHADOW_ATTENUATION(i);

				float lightIntensity = smoothstep(0, _ToonLine, ndotl * shadow);
				float4 light = lightIntensity * _LightColor0;

				//rim------------------------------------------
				float rim = 1 - dot(viewDir, normal);
				float rimIntensity = rim * pow(ndotl, _RimPow);
				rimIntensity = smoothstep(_RimAmount - 0.01, _RimAmount + 0.01, rimIntensity);
				float4 rimColor = rimIntensity * _RimColor;// *_SlowButton;

				//Dissolve alpha--------------------------
				if (noise.r >= _Cut)
				{
					alpha = 1;
				}
				else
				{
					alpha = 0;
				}

				//Dissolve OutThickness-------------------------
				if (noise.r >= _Cut * _OutThinkness)
					outline = 0;

				else
					outline = 1;

				//sick---------------------------------------
				float3 sickColor;
				float sick = saturate(dot(normal, viewDir));
				sickColor.rgb = _SickColor * _SickButton;

				//final ----------------------------------------------
				float4 final;
				final.rgb = (light + _AmbientColor + rimColor) * _Color * col+ sickColor;// +(outline * _OutColor.rgb);
				final.a = alpha;

				return final;
			}
			ENDCG


		}

		UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
		}
}