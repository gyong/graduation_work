﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterDance : MonoBehaviour
{
    Animator _anim;

    bool _isDance;

    private void Awake()
    {
        _isDance = false;
        _anim = GetComponent<Animator>();
        StartCoroutine(DanceStart());
    }

    private void Update()
    {
        _anim.SetBool("Dance", _isDance);
    }

    IEnumerator DanceStart()
    {
        yield return new WaitForSeconds(15.5f);
        _isDance = true;
    }
}
