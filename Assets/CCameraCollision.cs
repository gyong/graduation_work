﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCameraCollision : MonoBehaviour
{
    private static CCameraCollision _Instance = null;

    public static CCameraCollision Instance { get { return _Instance; } }
    public bool bIsCollision = false;

    private void Awake()
    {
        _Instance = this;
    }
    private void OnTriggerStay(Collider other)
    {
        bIsCollision = true;
    }

    private void OnTriggerExit(Collider other)
    {
        bIsCollision = false;
    }
}
