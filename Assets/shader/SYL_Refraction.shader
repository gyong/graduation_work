﻿Shader "Custom/SYL_Refraction"
{
    Properties
    {
		[HDR]_Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
		_RefTex ("Refraction Tex", 2D) = "Black" {}
		_Ref ("굴절되는 정도", Range(0,1)) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue"="Transparent"}
        LOD 200

			cull off


        CGPROGRAM


        #pragma surface surf Standard alpha:fade
        #pragma target 3.0

        sampler2D _MainTex, _RefTex;

        struct Input
        {
            float2 uv_MainTex;
			float2 uv_RefTex;
        };

        fixed4 _Color;
		float _Ref;


        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            fixed4 d = tex2D (_RefTex, IN.uv_RefTex);
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex + d.rg * _Ref ) * _Color;

            o.Albedo = c.rgb;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
