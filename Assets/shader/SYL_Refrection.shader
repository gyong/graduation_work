﻿Shader "Custom/SYL_Refrection"
{
    Properties
    {
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
		_MaskMap("Mask Map", 2D) = "White" {}
	}
		SubShader
	{
		Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }
		LOD 200

		GrabPass{}

		CGPROGRAM
		#pragma surface surf Lambert alpha:fade
		#pragma target 3.0

		sampler2D _MainTex, _GrabTexture, _MaskMap;

        struct Input
        {
            float2 uv_MainTex, uv_MaskMap;
			float4 screenPos;
        };



        void surf (Input IN, inout SurfaceOutput o)
        {
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex + _Time.y*0.1);
			float4 m = tex2D(_MaskMap, IN.uv_MaskMap);
			float3 scrPos = IN.screenPos.xyz / (IN.screenPos.w + 0.001);
			float3 grab = tex2D(_GrabTexture, scrPos.xy + c.r);

			o.Emission = grab * m.a;
			o.Alpha = m.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
